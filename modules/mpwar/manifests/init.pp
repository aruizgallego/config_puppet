# Miscellanius packages.
$misc_packages = ['vim-enhanced','telnet','zip','unzip','git','screen','libssh2','libssh2-devel','gcc','gcc-c++','autoconf','automake']
package { $misc_packages: ensure =>latest }


#Incluimos el modulo IPTABLES en el que hemos configurado que 
#permita puertos de entrada '80' y '8080'
include iptables


##INSTALAMOS APACHE
class { 'apache': 
}

##CREAMOS 2 VIRTUAL HOSTS 
apache::vhost { 'centos.dev':
	port => '80',
	docroot => '/www/',
	docroot_owner => 'vagrant',
	docroot_group => 'vagrant',
}

apache::vhost { 'proyect.dev':
	port => '80',
	docroot => '/www/proyect1/',
	docroot_owner => 'vagrant',
	docroot_group => 'vagrant',
}




##INSTALAMOS MYSQL
include '::mysql::server'

##CREAMOS LA BASE DE DATOS MPWAR_TEST
mysql::db { 'mpwar_test':
  user     => 'myuser',
  password => 'mypass',
  host     => 'localhost',
  grant    => ['SELECT', 'UPDATE'],
}






##INSTALAMOS PHP
class { 'php': 
	version => 'latest'}



#CREAMOS DOS HOSTS MYSQL1 y MEMCACHED1
host{ 'localhost':
	ensure => 'present',
	target => '/etc/hosts',
	ip => '127.0.0.1',
	host_aliases => ['mysql1','memcached1']
}


#Incluimos el modulo sudo !!INDEXMOD
include indexmod


#INSTALAMOS MEMCACHED
class { 'memcached':
}


#INSTALAMOS YUM EPEL
class { 'yum':
  extrarepo => [ 'epel' ],
}
